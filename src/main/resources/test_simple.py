#!/usr/bin/env python3

import os
import wave
from vosk import Model, KaldiRecognizer, SetLogLevel

SetLogLevel(0)

if not os.path.exists("model"):
    print(
        "Please download the model from https://alphacephei.com/vosk/models and unpack as 'model' in the current folder.")
    exit(1)
# sys.argv[1]
wf = wave.open("./test.wav", "rb")
if wf.getnchannels() != 1 or wf.getsampwidth() != 2 or wf.getcomptype() != "NONE":
    print("Audio file must be WAV format mono PCM.")
    exit(1)

model = Model("model")
rec = KaldiRecognizer(model, wf.getframerate())
rec.SetWords(False)

while True:
    data = wf.readframes(4000)
    if len(data) == 0:
        break
    if rec.AcceptWaveform(data):
        print("r" + rec.Result())
    else:
        print("p" + rec.PartialResult())

print("f" + rec.FinalResult())
